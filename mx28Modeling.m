nominalVoltage = 12;
rpmToRad = 2 * pi / 60;
radToEncoder = 4096 / (2 * pi);
pwmToVoltage = nominalVoltage / 511;
omegaNoLoad = 10600 * rpmToRad;
iNoLoad = 9.2 * 10^-3;
stallTorqueMotor = 15.5 * 10^-3;
stallTorqueServo = 2.5;
servo.P = 32;
servo.D = 0;
servo.I = 0;
servo.L = 0.206 * 10^-3;
servo.R = 8.3;
servo.Kt = 10.7 * 10^-3;
servo.Jm = 0.868 * 10^-3 * (10^-2)^2;
servo.Bm = servo.Kt * iNoLoad / omegaNoLoad;
servo.N = 193;
servo.eta = stallTorqueServo / (stallTorqueMotor * servo.N);

servo.JmGz = servo.Jm * servo.N^2 * servo.eta;
servo.BmGz = servo.Bm * servo.N^2 * servo.eta;
servo.BbackemfGz = servo.N^2 * servo.eta * servo.Kt^2 / servo.R;
servo.dampingGz = servo.BmGz + servo.BbackemfGz;
servo.Kp = (servo.P / 8);
servo.KpGz = radToEncoder * (servo.N * servo.eta * servo.Kt / servo.R) * (servo.P / 8) * pwmToVoltage;
servo.KdGz = radToEncoder * (servo.N * servo.eta * servo.Kt / servo.R) * (servo.D * 4 / 1000) * pwmToVoltage;
servo.KiGz = radToEncoder * (servo.N * servo.eta * servo.Kt / servo.R) * (servo.I * 1000 / 2048) * pwmToVoltage;
servo